#ifndef __VECTEUR__
#define __VECTEUR__

typedef enum{
    false,
    true
}Bool;

int* creerVecteur(int taille);

void afficherVecteur(int *vecteur, int taille);

int *initialiserVecteur(int taille);

Bool chercherElementVecteur(int element, int *vecteur, int taille);


#endif
#include <stdlib.h>
#include <stdio.h>
#include "vecteur.h"


int main(){
    int *data = initialiserVecteur(4);

    Bool trouve = chercherElementVecteur(12, data, 10);
    if(trouve)
        printf("Element trouvé\n");
    else
        printf("Element non trouvé\n");
    // afficherVecteur(data , 10);
    return 1;
}
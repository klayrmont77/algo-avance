#include <stdlib.h>
#include <stdio.h>
#include "vecteur.h"

void afficherVecteur(int *vecteur, int taille){
    int i = 0;
    for(i; i< taille; i++){
        printf("[%d]\n", vecteur[i]);
    }
}

int *creerVecteur(int taille){

    int *v = malloc(sizeof(int) * taille);
    return v;
}

int *initialiserVecteur(int taille){
    int *v = creerVecteur(taille);
    printf("Entrez les elements du vecteur en tapant entrer a chaque fois\n");
    for(int i = 0; i < taille; i++){
        scanf("%d", &v[i]);
    }
    return v;
}

Bool chercherElementVecteur(int element, int *vecteur, int taille){
    for(int i=0; i< taille; i++){
        if(vecteur[i] == element)
            return true;
    }
    return false;
}
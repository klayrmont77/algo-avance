#include <stdio.h>
#include <stdlib.h>
#include "rbtree.h"

void addNode(node **tree, unsigned int key)
{

    node *tmpNode;

    node *tmpTree = *tree;

    node *elem = malloc(sizeof(node));

    elem->key = key;

    elem->left = NULL;

    elem->right = NULL;

    if (tmpTree)

        do

        {

            tmpNode = tmpTree;

            if (key > tmpTree->key)

            {

                tmpTree = tmpTree->right;

                if (!tmpTree)
                    tmpNode->right = elem;
            }

            else

            {

                tmpTree = tmpTree->left;

                if (!tmpTree)
                    tmpNode->left = elem;
            }

        }

        while (tmpTree);

    else
        *tree = elem;
}

/***************************************************************************/

int searchNode(node *tree, unsigned int key)
{

    // while(tree)

    // {
    if (!tree)
        return 0;
    else
    {
        if (key == tree->key)
            return 1;
        if (key > tree->key)
            searchNode(tree->right, key);
        else
            searchNode(tree->left, key);
    }

    // }
}
/***************************************************************************/
void printTree(node *tree)
{
    if (!tree)
        return;
    if (tree->left)
        printTree(tree->left);
    printf("Cle = %d\n", tree->key);

    if (tree->right)
        printTree(tree->right);
}
/***************************************************************************/
void printReverseTree(node *tree)
{
    if (!tree)
        return;
    if (tree->right)
        printReverseTree(tree->right);
    printf("Cle = %d\n", tree->key);

    if (tree->left)
        printReverseTree(tree->left);
}
/***************************************************************************/
void clearTree(node **tree)
{
    node *tmpTree = *tree;
    if (!tree)
        return;
    if (tmpTree->left)
        clearTree(&tmpTree->left);
    if (tmpTree->right)
        clearTree(&tmpTree->right);
    free(tmpTree);

    *tree = NULL;
}

int nodeMin(node *tree)
{
    int min;
    if (!tree)
        return 0;
    node *tempLeft = tree->left;
    min = tempLeft->key;
    while (tempLeft)
    {
        // Parcours gauche
        if (min > tempLeft->key)
            min = tempLeft->key;
        tempLeft = tempLeft->left;
    }
    return min;
}

int nodeMax(node *tree)
{
    int max;
    if (!tree)
        return 0;
    node *tempRight = tree->right;
    max = tempRight->key;
    while (tempRight)
    {
        // Parcours droite
        if (max < tempRight->key)
            max = tempRight->key;
        tempRight = tempRight->right;
    }
    return max;
}

int nodeMaxRecursif(node *tree)
{
    if (!tree)
    {
        return 0;
    }
    node *tempRight = tree->right;
    if (!tempRight)
    {
        return tree->key;
    }
    else
    {
        return nodeMaxRecursif(tempRight);
    }
}

int nodeMinRecursif(node *tree)
{
    if (!tree)
    {
        return 0;
    }
    node *tempLeft = tree->left;
    if (!tempLeft)
    {
        return tree->key;
    }
    else
    {
        return nodeMinRecursif(tempLeft);
    }
}
#include <stdlib.h>
#include <stdio.h>
#include "rbtree.h"

int main()
{
    unsigned int Key;
    node *Arbre = NULL;
    addNode(&Arbre, 30);

    addNode(&Arbre, 20);

    addNode(&Arbre, 50);

    addNode(&Arbre, 45);

    addNode(&Arbre, 25);

    addNode(&Arbre, 80);

    addNode(&Arbre, 40);

    addNode(&Arbre, 70);

    addNode(&Arbre, 52);

    addNode(&Arbre, 10);
    puts("-------------------------------\n");
    // printTree(Arbre);
    printf("%d\n", nodeMinRecursif(Arbre));

    puts("-------------------------------");
    clearTree(&Arbre);
    return 0;
}
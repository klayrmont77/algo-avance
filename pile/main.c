#include <stdio.h>
#include <stdlib.h>
#include "stack.c"

int main(){
    Stack stack;
    stack = newStack();

    printStack(stack);

    stack = pushStack(stack, 14);
    stack = pushStack(stack, 18);
    stack = pushStack(stack, 17);
    stack = pushStack(stack, 47);
    stack = pushStack(stack, 15);
    stack = pushStack(stack, 11);

    printStack(stack);

    stack = popStack(stack);

    printf("====================\n");

    printStack(stack);

    stack = clearStack(stack);
    return 1;
}

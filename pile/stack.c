#include "stack.h"

Stack newStack(void){
    return NULL;
}

Bool isStackEmpty(Stack el){
    if(el == NULL)
        return true;
    return false;
}

Stack pushStack(Stack stack, int element){
    StackElement *stackResult;
    stackResult = malloc(sizeof(stackResult));
    if(stackResult == NULL){
        fprintf(stderr, "Probleme d'allocation dynamique\n");
        exit(EXIT_FAILURE);
    }
    stackResult->value = element;
    stackResult->next = stack;
    return stackResult;
}

Stack clearStack(Stack stack){
    StackElement *element;
    if(isStackEmpty(stack)){
        return newStack();
    }
    element = stack->next;
    free(stack);
    return clearStack(element);
}


void printStack(Stack stack){
    if(isStackEmpty(stack))
    {
        printf("Rien a affiche, la pile est vide\n");
        return;
    }
    while(!isStackEmpty(stack)){
        printf("[%d]\n", stack->value);
        stack = stack->next;
    }
}
Stack popStack(Stack stack){
    if(isStackEmpty(stack))
    {
        return newStack();
    }
    StackElement *element;
    element = stack->next;
    free(stack);
    return element;

}
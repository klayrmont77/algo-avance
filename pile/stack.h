#ifndef __STACK__H__
#define __STACK__H__

    typedef enum{
        false,
        true
    } Bool;

    // Definittion de la pile
    typedef struct StackElement{
        int value;
        struct StackElement *next;
    }StackElement, *Stack;

    // Prototype des fonctions
    Stack newStack(void);
    Bool isStackEmpty(Stack el);
    Stack pushStack(Stack stack, int element);
    Stack clearStack(Stack stack);
    void printStack(Stack stack);
    Stack popStack(Stack stack);
#endif